close all
% ��������� �������� RGB-����������� � ����������� ��� � ��������
biImg = imread('image.jpg'); 
biImg = rgb2gray(biImg);
level = graythresh(biImg);      % ��������� ���������� �� ���� (Otsu)
biImg = im2bw(biImg,level);
% ������� ��������� �� �����
figure(1)
imshow(biImg);
title(['Original binary image']);
% �������� ��� ������� �����������
[S1,S2,S3] =getShdwImgrac3x3opt(biImg);
% ������� �� �� �����
figure(2)
imshow(S1);
title(['Shadow image S1']);
% 
figure(3)
imshow(S2);
title(['Shadow image S2']);
%
figure(4)
imshow(S3);
title(['Shadow image S3']);
% ������� �� ����� ��������� �� ��������� ���� �� ����� 
% ���������� ���������
figure(5)
S12=xor(S1, S2);
S=imshow(~xor(S12, S3));% �������� �~�(NOT) ������������, ����� �������� �������� �����������,
% � �� ����������� � ��������� �������
title(['Superimposed image']);